/*
 *  morpheus-challenge-solution
 *  Copyright (C) 2018  Matthias Schwarz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcs

import com.gitlab.vitrox.mc.Challenge
import org.scalatest.{FlatSpec, Matchers}

class ChallengesSpec extends FlatSpec with Matchers {

  @inline private def receiveAFlagCheck(challenge: Challenge[_, _]): Unit = {
    challenge.getClass.getSimpleName should "receive a flag" in {
      challenge.flag shouldBe a [Some[_]]
    }
  }

  @inline private def receiveAFlagPercentageCheck(challenge: Challenge[_, _]): Unit = {
    challenge.getClass.getSimpleName should "receive a flag with a percentage of more than 50%" in {
      var counter = 0
      for (i <- 0 until 100) if (challenge.flag().isInstanceOf[Some[_]]) counter += 1
      counter should be > 50
    }
  }

  @inline private def benchmark(challenge: Challenge[_, _], repeats: Int): Unit = {
    challenge.getClass.getSimpleName should "benchmark" in {
      challenge.prettyBenchmark(repeats, System.err)
    }
  }

  receiveAFlagCheck(CC1Impl)
  receiveAFlagCheck(CC2Impl)
  receiveAFlagCheck(CC2SortedImpl)
  receiveAFlagCheck(CC3Impl)
  receiveAFlagCheck(CC4Impl)
  receiveAFlagPercentageCheck(CC5Impl)
  receiveAFlagCheck(CC6Impl)

}