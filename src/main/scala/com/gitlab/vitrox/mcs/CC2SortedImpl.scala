/*
 *  morpheus-challenge-solution
 *  Copyright (C) 2018  Matthias Schwarz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcs

import com.gitlab.vitrox.mcw.KList
import com.gitlab.vitrox.mcw.challenges.CC2Sorted

object CC2SortedImpl extends CC2Sorted {
  // Binary search algorithm
  override protected def solutionAlgorithm(i: KList[Long, Long]): Int = {
    var lower = 0
    var upper = i.list.length
    while (lower < upper) {
      val x = lower + (upper - lower) / 2
      val e = i.list(x)
      if (i.k == e) return x
      else if (i.k > e) lower = x
      else if (i.k < e) upper = x
    }
    -1
  }
}