/*
 *  morpheus-challenge-solution
 *  Copyright (C) 2018  Matthias Schwarz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcs

import com.gitlab.vitrox.mcw.challenges.CC5

import scala.collection.mutable

object CC5Impl extends CC5 {
  @inline private def calculation(func: (java.math.BigDecimal, java.math.BigDecimal) => java.math.BigDecimal)
                                 (implicit stack: mutable.ArrayStack[java.math.BigDecimal]): Unit = {
    val second = stack.pop
    val first = stack.pop
    stack.push(func(first, second))
  }

  override protected def solutionAlgorithm(i: Iterator[String]): Long = {
    implicit val stack: mutable.ArrayStack[java.math.BigDecimal] = new mutable.ArrayStack()
    import java.math.{MathContext, RoundingMode}
    while (i.hasNext) {
      val next = i.next
      next match {
        case "+" => calculation((f, s)=>f.add(s))
        case "-" => calculation((f, s)=>f.subtract(s))
        case "*" => calculation((f, s)=>f.multiply(s))
        case "/" => calculation((f, s)=>f.divide(s, 64, RoundingMode.HALF_EVEN))
        case _ => stack.push(new java.math.BigDecimal(next))
      }
    }
    stack.pop.round(new MathContext(64, RoundingMode.DOWN)).longValue
  }
}