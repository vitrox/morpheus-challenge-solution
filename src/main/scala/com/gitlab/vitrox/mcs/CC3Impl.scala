/*
 *  morpheus-challenge-solution
 *  Copyright (C) 2018  Matthias Schwarz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcs

import com.gitlab.vitrox.mcw.KList
import com.gitlab.vitrox.mcw.challenges.CC3

object CC3Impl extends CC3 {
  override protected def solutionAlgorithm(i: KList[Int, Long]): Long = {
    import java.util
    val set: util.TreeSet[Long] = new util.TreeSet[Long]()
    for (index <- 0 until i.k) set.add(i.list(index))
    for (index <- i.k until i.list.length) {
      if (i.list(index) > set.first) {
        set.pollFirst
        set.add(i.list(index))
      }
    }
    set.first
  }
}