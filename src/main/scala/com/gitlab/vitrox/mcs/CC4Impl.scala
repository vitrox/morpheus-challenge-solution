/*
 *  morpheus-challenge-solution
 *  Copyright (C) 2018  Matthias Schwarz
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.mcs

import com.gitlab.vitrox.mcw.KList
import com.gitlab.vitrox.mcw.challenges.CC4

object CC4Impl extends CC4 {
  override protected def solutionAlgorithm(i: KList[Int, String]): Iterable[String] = {
    val offset = i.list.length - (i.k % i.list.length)
    val result = new scala.collection.mutable.MutableList[String]()
    for (index <- offset until i.list.length) result += i.list(index)
    for (index <- 0 until offset) result += i.list(index)
    result
  }
}