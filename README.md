# morpheus-challenge-solution (mcs)

Contains solutions for the challenge api provided by the german youtuber 
"[TheMorpheus407](https://www.youtube.com/user/TheMorpheus407)".

The solutions are mostly self implemented although there are build-in options for it.

Performance it not the goal in these solutions, the target is to learn and practice coding algorithms.

## Credits

Huge thanks to the german youtuber "[TheMorpheus407](https://www.youtube.com/user/TheMorpheus407)" and his community
for these challenges.