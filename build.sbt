ThisBuild / organization := "com.gitlab.vitrox"
ThisBuild / scalaVersion := "2.12.6"
ThisBuild /    resolvers += "jitpack" at "https://jitpack.io"

lazy val root = (project in file("."))
  .settings(
    name                := "morpheus-challenge-solution",
    version             := "0.1.0",
    libraryDependencies ++= Seq(
      "com.gitlab.vitrox" % "morpheus-challenge-wrapper" % "0.6.0",
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
    )
  )